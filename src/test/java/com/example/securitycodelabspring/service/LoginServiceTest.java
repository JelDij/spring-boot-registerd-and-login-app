package com.example.securitycodelabspring.service;

import com.example.securitycodelabspring.domain.dao.TokenDAO;
import com.example.securitycodelabspring.domain.dao.UserDAO;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
class LoginServiceTest {

    private LoginService loginService;
    private RegistrationService registrationService;
    private TokenDAO tokenDAO;
    private static UserDAO userDAO;

    @Autowired
    public LoginServiceTest(LoginService loginService, RegistrationService registrationService, TokenDAO tokenDAO, UserDAO userDAO) {
        this.loginService = loginService;
        this.registrationService = registrationService;
        this.tokenDAO = tokenDAO;
        this.userDAO = userDAO;
    }

    /**
     * For our test, we register a user who is deleted after we run the tests
     * @throws NoSuchAlgorithmException
     */
    @BeforeEach
    public void registerTestUser() throws NoSuchAlgorithmException {
        registrationService.register("Maria", "test01");
    }

    @Test
    public void LoginServiceAvailable() {
        assertNotNull(loginService);
    }


    @Test
    public void loginTest() throws NoSuchAlgorithmException {
        String actual = loginService.login("Maria", "test01");
        String expected = tokenDAO.findTokenByUser(userDAO.findUserByUsername("Maria"));
        assertEquals(actual, expected);
    }

    /**
     * for test purposes, we delete the token everytime we ran a test
     */
    @AfterEach
    public void deleteTokens() {
        tokenDAO.deleteTokenByUserId(userDAO.findUserByUsername("Maria").getUserId());
    }

    /**
     * We delete the user used in the test
     */
    @AfterAll
    public static void deleteUser() {
        userDAO.deleteUserByUsername("Maria");
    }


}