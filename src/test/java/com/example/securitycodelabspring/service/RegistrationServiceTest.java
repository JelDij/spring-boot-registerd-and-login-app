package com.example.securitycodelabspring.service;
import com.example.securitycodelabspring.domain.dao.UserDAO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.security.NoSuchAlgorithmException;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
class RegistrationServiceTest {

    private RegistrationService registrationService;
    private UserDAO userDao;
    private HashService hashService;

    @Autowired
    public RegistrationServiceTest(RegistrationService registrationService, UserDAO userDao, HashService hashService) {
        super();
        this.registrationService = registrationService;
        this.userDao = userDao;
        this.hashService = hashService;
    }

    @Test
    public void RegistrationServiceAvailable() {
        assertNotNull(registrationService);
    }

    // Test to assert that simply hashing the given password will NOT result in a match with the database
    @Test
    public void registerTestPasswordHashedCorrectly() throws NoSuchAlgorithmException {
        registrationService.register("Jelmer", "test01");
        String expected = hashService.hash("test01");
        String actual = userDao.findHashByUsername("Jelmer");
        assertNotEquals(expected, actual);
    }

    // Test combined the salt with the password, this should be equal
    @Test
    public void registerTestSaltAndPasswordHashedCorrectly() throws NoSuchAlgorithmException {
        registrationService.register("Pietje", "test02");
        String expected = hashService.hash("test02" + userDao.findSaltByUsename("Pietje"));
        String actual = userDao.findHashByUsername("Pietje");
        assertEquals(expected, actual);
        }
    }

