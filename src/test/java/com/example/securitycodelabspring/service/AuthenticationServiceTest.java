package com.example.securitycodelabspring.service;

import com.example.securitycodelabspring.domain.dao.TokenDAO;
import com.example.securitycodelabspring.domain.dao.UserDAO;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

@SpringBootTest
@ActiveProfiles("test")
class AuthenticationServiceTest {

    private AuthenticationService authenticationService;
    private RegistrationService registrationService;
    private LoginService loginService;
    private TokenDAO tokenDAO;
    private UserDAO userDao;
    private HashService hashService;

    @Autowired
    public void AuthenticationService(AuthenticationService authenticationService, RegistrationService registrationService,
                                      LoginService loginService, TokenDAO tokenDAO, UserDAO userDAO, HashService hashService) {
        this.authenticationService = authenticationService;
        this.registrationService = registrationService;
        this.loginService = loginService;
        this.tokenDAO = tokenDAO;
        this.userDao = userDAO;
        this.hashService = hashService;
    }

    @Test
    public void AuthenticationServiceAvailable() {
        assertNotNull(authenticationService);
    }

    @Test
    public void testAuthenticateUser() throws NoSuchAlgorithmException {
        registrationService.register("Dirk", "test01");
        boolean actual = authenticationService.authenticate("Dirk", "test01");
        boolean expected = true;
        assertEquals(actual, expected);
    }

    /**
     * This test is not well designed; we need to think how to design this; how can this effectively be tested?
     * In the current test our expected does basically the same as the method called in the actual
     * @throws NoSuchAlgorithmException
     */
    @Test
    public void testAuthenticateToken() throws NoSuchAlgorithmException {
        String token = loginService.login("Dirk", "test01");
        LocalDateTime tokenTime = tokenDAO.findGeneratedDateOfToken(token);
        boolean actual = authenticationService.authenticate(token);
        long hoursPassed = ChronoUnit.HOURS.between(tokenTime, LocalDateTime.now());
        boolean expected = (hoursPassed < 5) && tokenDAO.checkIfDatabaseContainsToken(token);
        assertEquals(actual, expected);
    }



    /**
     * for test purposes, we delete the token everytime we ran a test
     */
    @AfterEach
    public void deleteTokens() {
        tokenDAO.deleteTokenByUserId(userDao.findUserByUsername("Dirk").getUserId());
    }

}