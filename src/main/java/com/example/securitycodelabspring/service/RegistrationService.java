package com.example.securitycodelabspring.service;

import com.example.securitycodelabspring.domain.dao.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.security.NoSuchAlgorithmException;

/**
 * Author: Jelmer Dijkstra 07-05-2021
 * Service class used by RegisterController to register new user.
 */

@Service
public class RegistrationService {

    private UserDAO userDAO;
    private HashService hashService;


    @Autowired
    public RegistrationService(UserDAO userDAO, HashService hashService) {
        this.userDAO = userDAO;
        this.hashService = hashService;
    }

    public boolean register(String username, String password) throws NoSuchAlgorithmException {
        String salt = hashService.salt();
        String hash = hashService.hash(password + salt);
        if (userDAO.insertUserNameWithHashAndSalt(username, hash, salt)) {
            System.out.println("Gebruiker toegevoegd");
            return true;
        } else {
            System.out.println("Gebruiker niet toegevoegd");
            return false;
        }
    }
}
