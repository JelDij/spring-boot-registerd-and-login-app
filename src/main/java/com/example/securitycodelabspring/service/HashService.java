package com.example.securitycodelabspring.service;

import com.example.securitycodelabspring.utility.HashHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.security.NoSuchAlgorithmException;

/**
 * Author: Jelmer Dijkstra 07-05-2021
 * Service class used to hash password and salt. Uses utility class to do actual hashing.
 */

@Service
public class HashService {

    private static final int DEFAULT_ROUNDS = 2;
    private PepperService pepperService;
    private SaltService saltService;
    private int rounds;

    @Autowired
    public HashService(PepperService pepperService, SaltService saltService) {
        super();
        this.pepperService = pepperService;
        this.saltService = saltService;
        this.rounds = DEFAULT_ROUNDS;
    }

    // pepper wordt 'meegehashed' en wordt met het wachtwoord opgeslagen
    public String hash(String passwordPlusSalt) throws NoSuchAlgorithmException {
        String hash = HashHelper.hash(passwordPlusSalt + pepperService.getPepper());
        return processRonds(hash, numberOfRounds(rounds));
    }

    // salt apart gegenereerd want moet apart opgeslagen worden
    public String salt() {
        return saltService.generateSalt();
    }

    private String processRonds(String hash, long rounds) throws NoSuchAlgorithmException {
        for (long i = 0; i < rounds; i++) {
            hash = HashHelper.hash(hash);
        }
        return hash;
    }

    private long numberOfRounds(int load){
        int base = 10;
        long result = base; // base ^ 1

        for (int i = 0; i < load; i++) {
            result *= base;
        }
        return result;
    }

}
