package com.example.securitycodelabspring.service;

import org.springframework.stereotype.Service;

/**
 * Author: Jelmer Dijkstra 07-05-2021
 * Service class which returns the Pepper of this application. Pepper is not stored elsewhere.
 */

@Service
public class PepperService {

    private final String PEPPER = "2998563f254e280eb14ded29f23b";

    public String getPepper() {
        return PEPPER;
    }

}
