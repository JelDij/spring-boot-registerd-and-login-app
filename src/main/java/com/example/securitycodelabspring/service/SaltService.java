package com.example.securitycodelabspring.service;

import com.example.securitycodelabspring.utility.ByteToArrayToHexHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.security.SecureRandom;

/**
 * Author: Jelmer Dijkstra 07-05-2021
 * Service class which generates a Salt for a user. Uses utilityclasses for hashing.
 */

@Service
public class SaltService {

    private final int SALT_LENGHT = 8;

    @Autowired
    public SaltService() {
        super();
    }

    public String generateSalt() {
        SecureRandom srng = new SecureRandom();
        byte[] byteArray = new byte[SALT_LENGHT];
        srng.nextBytes(byteArray);
        return ByteToArrayToHexHelper.encodeHexString(byteArray);
    }
}
