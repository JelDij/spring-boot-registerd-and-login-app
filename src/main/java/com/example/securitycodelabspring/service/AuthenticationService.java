package com.example.securitycodelabspring.service;

import com.example.securitycodelabspring.domain.dao.TokenDAO;
import com.example.securitycodelabspring.domain.dao.UserDAO;
import com.example.securitycodelabspring.domain.model.Credentials;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;

/**
 * Author: Jelmer Dijkstra 07-05-2021
 * Service class used to authenticate a login
 */

@Service
@Component
public class AuthenticationService {

    private UserDAO userDAO;
    private TokenDAO tokenDAO;
    private HashService hashService;
    private final int DURATION_TOKEN_IS_VALID = 5;

    @Autowired
    public AuthenticationService(UserDAO userDAO, TokenDAO tokenDAO, HashService hashService) {
        this.userDAO = userDAO;
        this.tokenDAO = tokenDAO;
        this.hashService = hashService;
    }

    /**
     * Authentication service which accepts two strings
     * @param username
     * @param password
     * @return
     * @throws NoSuchAlgorithmException
     */
    public boolean authenticate(String username, String password) throws NoSuchAlgorithmException {
        if (userDAO.findHashByUsername(username) != null) {
            if (userDAO.findHashByUsername(username).equals(hashService.hash(password + userDAO.findSaltByUsename(username)))) {
                return true;
            } else {
                return false;
            }
        } return false;
    }

    /**
     * Authentication service which accepts JSON object credentials, not used in this project.
     * @param credentials
     * @return
     * @throws NoSuchAlgorithmException
     */
    public boolean authenticate(Credentials credentials) throws NoSuchAlgorithmException {
        if (userDAO.findHashByUsername(credentials.getUsername()) != null) {
            if (userDAO.findHashByUsername(credentials.getUsername()).equals(hashService.hash(credentials.getPassword()) + userDAO.findSaltByUsename(credentials.getUsername()))) {
                return true;
            } else {
                return false;
            }
        } return false;
    }

    public boolean authenticate(String token) {
        if (tokenDAO.checkIfDatabaseContainsToken(token) && checkIfTokenIsValid(token)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean checkIfTokenIsValid(String token) {
        LocalDateTime tokenTime = tokenDAO.findGeneratedDateOfToken(token);
        long hoursPassed = ChronoUnit.HOURS.between(tokenTime, LocalDateTime.now());
        return hoursPassed < DURATION_TOKEN_IS_VALID;
    }

}
