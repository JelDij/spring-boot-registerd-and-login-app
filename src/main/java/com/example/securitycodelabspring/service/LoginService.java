package com.example.securitycodelabspring.service;

import com.example.securitycodelabspring.domain.dao.TokenDAO;
import com.example.securitycodelabspring.domain.dao.UserDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.security.NoSuchAlgorithmException;
import java.util.UUID;

/**
 * Author: Jelmer Dijkstra 07-05-2021
 * Service class used by LoginController; returns and saves a user Token.
 */

@Service
public class LoginService {

    private UserDAO userDAO;
    private TokenDAO tokenDAO;
    private AuthenticationService authenticationService;

    @Autowired
    public LoginService(UserDAO userDAO, TokenDAO tokenDAO, AuthenticationService authenticationService) {
        this.userDAO = userDAO;
        this.tokenDAO = tokenDAO;
        this.authenticationService = authenticationService;
    }

    /**
     * Method returns token when first log in
     * @param username
     * @param password
     * @return
     * @throws NoSuchAlgorithmException
     */
    public String login(String username, String password) throws NoSuchAlgorithmException {
        if (authenticationService.authenticate(username, password)) {
            String token = UUID.randomUUID().toString();
            if (tokenDAO.findTokenByUserId(userDAO.findUserByUsername(username).getUserId()) == null) {
                tokenDAO.insertToken(userDAO.findUserByUsername(username), token);
                return token;
            } else {
                tokenDAO.updateToken(username, token);
                return token;
            }
        } else {
            return "Gebruikersnaam of wachtwoord onjuist";
        }
    }
}
