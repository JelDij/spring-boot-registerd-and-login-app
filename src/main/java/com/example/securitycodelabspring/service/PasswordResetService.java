package com.example.securitycodelabspring.service;

import com.example.securitycodelabspring.domain.dao.UserDAO;
import com.example.securitycodelabspring.domain.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.security.NoSuchAlgorithmException;

/**
 * Author: Jelmer Dijkstra 07-05-2021
 * Service class used to resetPassword; not used in this project
 */

@Service
public class PasswordResetService {

    private UserDAO userDao;
    private HashService hashService;

    @Autowired
    public PasswordResetService(UserDAO userDao, HashService hashService) {
        this.userDao = userDao;
        this.hashService = hashService;
    }

    // voor nu uitgecomment, want email zit niet in deze applicatie
//    public void sendResetMail(User user) {
//        if (userDao.findUserByUsername(user.getUsername()) != null) {
//            emailService.sendSimpleMail(user.getEmail, "Password Reset", "reset");
//        }
//    }

    /**
     * This method resets the password
     * @param user
     * @param password
     * @throws NoSuchAlgorithmException
     */
    public void resetPassword(User user, String password) throws NoSuchAlgorithmException {
        String salt = user.getSalt();
        String hash = hashService.hash(password + salt);
        userDao.resetPassword(user.getUsername(), hash);
    }
}
