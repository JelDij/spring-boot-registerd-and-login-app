package com.example.securitycodelabspring.controller;

import com.example.securitycodelabspring.domain.model.Credentials;
import com.example.securitycodelabspring.service.RegistrationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.security.NoSuchAlgorithmException;
import com.google.gson.Gson;

/**
 * Author: Jelmer Dijkstra 07-05-2021
 * Controller to register new user
 */

@RestController
public class RegisterController {

    private RegistrationService registrationService;


    @Autowired
    public RegisterController(RegistrationService registrationService) {
        super();
        this.registrationService = registrationService;
    }


    /**
     * Method to register new user. Returns a 201 code if created, 409 when user already exists
     * @param credentials
     * @return
     * @throws NoSuchAlgorithmException
     */
    @PostMapping("/newuser") // http://localhost:8080/newuser
    @ResponseBody
    public ResponseEntity<?> newUserHandler(@RequestBody Credentials credentials) throws NoSuchAlgorithmException {
        Gson gson = new Gson();
        StringBuilder returnMessage = new StringBuilder("message: ");
        if (registrationService.register(credentials.getUsername(), credentials.getPassword())) {
            returnMessage.append("Username: " + credentials.getUsername() + " opgeslagen");
            return ResponseEntity.status(HttpStatus.CREATED).body(gson.toJson(returnMessage));
        } else {
            returnMessage.append("Username: " + credentials.getUsername() + " al in gebruik");
            return ResponseEntity.status(HttpStatus.CONFLICT).body(gson.toJson(returnMessage));
        }
    }
}
