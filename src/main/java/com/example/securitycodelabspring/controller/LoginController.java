package com.example.securitycodelabspring.controller;

import com.example.securitycodelabspring.domain.model.Credentials;
import com.example.securitycodelabspring.service.LoginService;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import java.security.NoSuchAlgorithmException;

/**
 * Author: Jelmer Dijkstra 07-05-2021
 * Controller to login user
 */
@RestController
public class LoginController {

    private LoginService loginService;

    @Autowired
    public LoginController(LoginService loginService) {
        this.loginService = loginService;
    }

    /**
     * loginUser is used to login to server. Response is the created Token. This is send back for testing purposes here
     * @param credentials
     * @return
     * @throws NoSuchAlgorithmException
     */
    @PostMapping("/loginuser") // http://localhost:8080/loginuser
    @ResponseBody
    public ResponseEntity<?> loginUser(@RequestBody Credentials credentials) throws NoSuchAlgorithmException {
        String response = loginService.login(credentials.getUsername(), credentials.getPassword());
        Gson gson = new Gson();
        StringBuilder returnMessage = new StringBuilder("message: ");
        if (!response.equals("Gebruikersnaam of wachtwoord onjuist")) {
            returnMessage.append("Welkom, " + credentials.getUsername());
            return ResponseEntity.status(HttpStatus.OK).header("Authorization", response).body(gson.toJson(returnMessage));
        } else {
            returnMessage.append("Authenticatie niet geslaagd");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(returnMessage);
        }
    }
}

