package com.example.securitycodelabspring.controller;

import com.example.securitycodelabspring.utility.EmailConfiguration;
import com.example.securitycodelabspring.domain.model.EmailFeedback;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSenderImpl;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.validation.ValidationException;

/**
 * Author: Jelmer Dijkstra, 07-05-2021
 * Controller which is used when the user wants to send a 'feedback' email. See class EmailFeedback
 */

@RestController
@RequestMapping("/feedback")
public class FeedbackController {

    private EmailConfiguration emailConfiguration;

    @Autowired
    public FeedbackController(EmailConfiguration emailConfiguration) {
        this.emailConfiguration = emailConfiguration;
    }

    @PostMapping
    public void sendFeedback(@RequestBody EmailFeedback feedback, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            throw new ValidationException("Feedback is not valid");
        }
        // Create a mail sender with configuration
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(this.emailConfiguration.getHost());
        mailSender.setPort(this.emailConfiguration.getPort());
        mailSender.setUsername(this.emailConfiguration.getUsername());
        mailSender.setPassword(this.emailConfiguration.getPassword());

        // Create an email instance
        SimpleMailMessage mailMessage = new SimpleMailMessage();
        mailMessage.setFrom(feedback.getEmail());
        mailMessage.setTo("noreply@mailtrap.io");
        mailMessage.setSubject("New Feedback from " + feedback.getName());
        mailMessage.setText(feedback.getFeedback());

        // Send mail
        mailSender.send(mailMessage);
    }

}
