package com.example.securitycodelabspring.utility;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Utility class which is used to Hash a password. Used "SHA-256". Converts bytearray to
 * String by using ByteToArrayToHexHelper.
 */
public class HashHelper {

    public static String hash(String hash) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(hash.getBytes(StandardCharsets.UTF_8));
        byte[] digest = md.digest();
        return ByteToArrayToHexHelper.encodeHexString(digest);
    }
}

