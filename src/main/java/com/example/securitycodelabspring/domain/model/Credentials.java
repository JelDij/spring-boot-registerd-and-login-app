package com.example.securitycodelabspring.domain.model;

/**
 * Author: Jelmer Dijkstra 07-05-2021
 * Class used in login and register controllers.
 */

public class Credentials {

    private String username;
    private String password;

    public Credentials(String username, String password) {
        super();
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

}
