package com.example.securitycodelabspring.domain.model;

import java.sql.Date;
import java.time.LocalDateTime;

/**
 * Author: Jelmer Dijkstra 07-05-2021
 * Class for Tokens.
 */

public class Token {

    private User user;
    private String token;
    private LocalDateTime timeGenerateToken;

    public Token(User user, String token, LocalDateTime timeGenerateToken) {
        this.user = user;
        this.token = token;
        this.timeGenerateToken = timeGenerateToken;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getToken() {
        return token;
    }

    public LocalDateTime getTimeGenerateToken() {
        return timeGenerateToken;
    }

}
