package com.example.securitycodelabspring.domain.model;


import javax.validation.constraints.Email;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * Author: Jelmer Dijkstra, 07-05-2021
 * Class which can be used to send an email
 */

public class EmailFeedback {

    @NotNull
    private String name;

    @NotNull
    @Email
    private String email;

    @NotNull
    @Min(10)
    private String feedback;

    public String getName() {
        return name;
    }


    public String getEmail() {
        return email;
    }


    public String getFeedback() {
        return feedback;
    }

}
