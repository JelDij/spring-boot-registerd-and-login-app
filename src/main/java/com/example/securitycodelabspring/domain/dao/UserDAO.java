package com.example.securitycodelabspring.domain.dao;

import com.example.securitycodelabspring.domain.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.stereotype.Repository;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * Author: Jelmer Dijkstra 07-05-2021
 * Database acces for User, used for register etc.
 */

@Repository
public class UserDAO {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public UserDAO(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public String findHashByUsername(String username) {
        List<User> tempuser = jdbcTemplate.query("Select * from security_codelab.user " +
                "where username = ?", new UserRowMapper(), username);
        if (tempuser.isEmpty()) {
            return null;
        } else {
            return tempuser.get(0).getPasswordHash();
        }
    }

    public String findSaltByUsename(String username) {
        List <User> tempuser = jdbcTemplate.query("Select * from security_codelab.user where username = ?", new UserRowMapper(), username);
        if (tempuser.isEmpty()) {
            return null;
        } else {
            return tempuser.get(0).getSalt();
        }
    }

    public User findUserById(int userId) {
        List<User> tempuser = jdbcTemplate.query("Select * from security_codelab.user where userId = ?", new UserRowMapper(), userId);
        if (tempuser.size() !=  1) {
            return null;
        } else {
            return tempuser.get(0);
        }
    }

    public User findUserByUsername(String username) {
        List<User> tempuser = jdbcTemplate.query("Select * from security_codelab.user where username = ?", new UserRowMapper(), username);
        if (tempuser.size() !=  1) {
            return null;
        } else {
            return tempuser.get(0);
        }
    }

    public boolean insertUserNameWithHashAndSalt(String username, String hash, String salt) {
        if (findHashByUsername(username) == null) {
            String sql = "insert into security_codelab.user(username, hash, salt) values (?, ?, ?)";
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(sql, new String[]{"id"});
                    ps.setString(1, username);
                    ps.setString(2, hash);
                    ps.setString(3, salt);
                    return ps;
                }
            });
            return true;
        }
        return false;
    }

    public void resetPassword(String username, String hash) {
        if (findHashByUsername(username) != null) {
            jdbcTemplate.update("update security_codelab.user set hash = ? where username = ?", hash, username);
        }
    }

    public void deleteUserByUsername(String username) {
        if (findUserByUsername(username) != null) {
            jdbcTemplate.update("delete from security_codelab.user where username = ?", username);
        }
    }
}
