package com.example.securitycodelabspring.domain.dao;

import com.example.securitycodelabspring.domain.model.Token;
import com.example.securitycodelabspring.domain.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.stereotype.Repository;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * Author: Jelmer Dijkstra 07-05-2021
 * Database acces for tokens. Some methods are not used, but written for future use in other projects.
 */
@Repository
public class TokenDAO {

    private final JdbcTemplate jdbcTemplate;
    private UserDAO userDAO;

    @Autowired
    public TokenDAO(JdbcTemplate jdbcTemplate, UserDAO userDAO) {
        this.jdbcTemplate = jdbcTemplate;
        this.userDAO = userDAO;
    }

    public void insertToken(User user, String token) {
        if (userDAO.findHashByUsername(user.getUsername()) != null) {
            String sql = "insert into security_codelab.token(userId, token, time) values (?, ?, CURRENT_TIME())";
            jdbcTemplate.update(new PreparedStatementCreator() {
                @Override
                public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                    PreparedStatement ps = connection.prepareStatement(sql);
                    ps.setInt(1, userDAO.findUserByUsername(user.getUsername()).getUserId());
                    ps.setString(2, token);
                    return ps;
                }
            });
        }
    }

    public void updateToken(String username, String token) {
        jdbcTemplate.update("update security_codelab.token set token = ?, time = CURRENT_TIME() where userId = ?",
                token, userDAO.findUserByUsername(username).getUserId());
    }

    public String findTokenByUser(User user) {
        List<Token> tempToken = jdbcTemplate.query("select * from security_codelab.token where userId = ?", new TokenRowMapper(userDAO), user.getUserId());
        if (tempToken.isEmpty()) {
            return null;
        } else {
            return tempToken.get(0).getToken();
        }
    }

    public String findTokenByUserId(int userId) {
        List<Token> tempToken = jdbcTemplate.query("select * from security_codelab.token where userId = ?", new TokenRowMapper(userDAO), userId);
        if (tempToken.isEmpty()) {
            return null;
        } else {
            return tempToken.get(0).getToken();
        }
    }

    public LocalDateTime findGenerateDateOfTokenByUser(User user) {
        List<Token> tempToken = jdbcTemplate.query("select * from security_codelab.token where userId = ?", new TokenRowMapper(userDAO), user.getUserId());
        if (tempToken.isEmpty()) {
            return null;
        } else {
            return tempToken.get(0).getTimeGenerateToken();
        }
    }

    public LocalDateTime findGenerateDateOfTokenByUserId(int userId) {
        List<Token> tempToken = jdbcTemplate.query("select * from security_codelab.token where userId = ?", new TokenRowMapper(userDAO), userId);
        if (tempToken.isEmpty()) {
            return null;
        } else {
            return tempToken.get(0).getTimeGenerateToken();
        }
    }

    public LocalDateTime findGeneratedDateOfToken(String token) {
        List<Token> tempToken = jdbcTemplate.query("select * from security_codelab.token where token = ?", new TokenRowMapper(userDAO), token);
        if (tempToken.isEmpty()) {
            return null;
        } else {
            return tempToken.get(0).getTimeGenerateToken();
        }
    }

    public boolean checkIfDatabaseContainsToken(String token) {
        List<Token> tempToken = jdbcTemplate.query("select * from security_codelab.token where token = ?", new TokenRowMapper(userDAO), token);
        if (tempToken.isEmpty()) {
            return false;
        } else {
            return true;
        }
    }

    public void deleteTokenByUserId(int userId) {
        if (findTokenByUserId(userId) != null) {
            jdbcTemplate.update("delete from security_codelab.token where userId = ?", userId);
        }
    }
}
