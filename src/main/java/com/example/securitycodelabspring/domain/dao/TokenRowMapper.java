package com.example.securitycodelabspring.domain.dao;

import com.example.securitycodelabspring.domain.model.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Author: Jelmer Dijkstra 07-05-2021
 * RowMapper used in TokenDao
 */

public class TokenRowMapper implements RowMapper<Token> {

    private UserDAO userDAO;

    @Autowired
    public TokenRowMapper(UserDAO userDAO) {
        this.userDAO = userDAO;
    }

    @Override
    public Token mapRow(ResultSet resultSet, int i) throws SQLException {

        Token tempToken = new Token(userDAO.findUserById(resultSet.getInt("userId")),
                resultSet.getString("token"), resultSet.getTimestamp("time").toLocalDateTime());
        return tempToken;
    }
}
