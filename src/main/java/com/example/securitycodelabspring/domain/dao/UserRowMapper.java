package com.example.securitycodelabspring.domain.dao;

import com.example.securitycodelabspring.domain.model.User;
import org.springframework.jdbc.core.RowMapper;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Author: Jelmer Dijkstra 07-05-2021
 * RowMapper used in UserDao
 */

public class UserRowMapper implements RowMapper<User> {

    @Override
    public User mapRow(ResultSet resultSet, int i) throws SQLException {
        User tempUser = new User(resultSet.getString("username"));
        tempUser.setPasswordHash(resultSet.getString("hash"));
        tempUser.setSalt(resultSet.getString("salt"));
        tempUser.setUserId(resultSet.getInt("userId"));
        return tempUser;
    }
}
