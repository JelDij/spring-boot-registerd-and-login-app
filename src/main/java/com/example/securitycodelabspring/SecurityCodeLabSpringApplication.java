package com.example.securitycodelabspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Author: Jelmer Dijkstra 07-05-2021
 * Launch SpringBoot
 */

@SpringBootApplication
public class SecurityCodeLabSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(SecurityCodeLabSpringApplication.class, args);
    }

}
