## Schema to create simple database used in this application
CREATE SCHEMA IF NOT EXISTS `security_codelab`;
CREATE TABLE IF NOT EXISTS `security_codelab`.`user` (
                                                        `userId` INT NOT NULL AUTO_INCREMENT,
                                                        `username` VARCHAR(45) NOT NULL,
                                                        `hash` VARCHAR(100) NOT NULL,
                                                        `salt` VARCHAR(100) NOT NULL,
                                                        PRIMARY KEY (`userId`));

CREATE TABLE IF NOT EXISTS `security_codelab`.`token` (
                                                         `userId` INT NOT NULL,
                                                         `token` VARCHAR(100) NOT NULL,
                                                         `time` DATETIME NOT NULL,
                                                         PRIMARY KEY (`userId`),
                                                         CONSTRAINT
                                                            FOREIGN KEY (`userId`)
                                                            REFERENCES `security_codelab`.user (`userId`)
                                                            ON DELETE CASCADE
                                                            ON UPDATE CASCADE )

-- -----------------------------------------------------
-- Create User  `userBankMeLater`@`localhost`
-- -----------------------------------------------------
DROP USER IF EXISTS 'user_security_codelab'@'localhost';
CREATE USER 'user_security_codelab'@'localhost' IDENTIFIED BY 'user1';
GRANT ALL PRIVILEGES ON security_codelab.* TO 'user_security_codelab'@'localhost';

